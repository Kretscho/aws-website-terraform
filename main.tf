// This block tells Terraform that we're going to provision AWS resources.
provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket = "terraform-state.kretsch.men"
    key = "website/kretsch.men/terraform.tfstate"
    region = "us-east-1"
  }
}

locals {
  env= terraform.workspace
  counts = {
    "production"=1
    "lena"=2
    "hybrid"=3
  }
  prod_env = lookup(local.counts,local.env)
  lena_env = lookup(local.counts,local.env)
  hybrid_env = lookup(local.counts,local.env)


  alias_list = {
    "production"=[var.www_domain_name, var.root_domain_name]
    "lena"=[var.www_domain_name, var.root_domain_name]
    "hybrid"=[var.www_domain_name, var.root_domain_name]
  }
  cloudfront_alias_list = lookup(local.alias_list,local.env)
}

// Create a variable for our domain name because we'll be using it a lot.
variable "www_domain_name" {
  description = "domain name with subdomain e.g. test.rootdomain.com"
}

// Create a variable for our domain name because we'll be using it a lot.
variable "index_file" {
  description = "index file in cloudfront"
}

// We'll also need the root domain (also known as zone apex or naked domain).
variable "root_domain_name" {
  description = "the root domain name e.g. kretsch.men"
}

// The S3 bucket that we use as a bases for all web files
resource "aws_s3_bucket" "www" {
  bucket = var.www_domain_name
}

// These right settings are required to access the bucket from a CloudFront distribution
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "Our Origin"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.www.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [
        aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.www.arn]

    principals {
      type        = "AWS"
      identifiers = [
        aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "www" {
  bucket = aws_s3_bucket.www.id
  policy = data.aws_iam_policy_document.s3_policy.json
}

# ACM Certificate generation
resource "aws_acm_certificate" "certificate" {
  domain_name       = var.root_domain_name
  validation_method = "DNS"
  subject_alternative_names = [var.www_domain_name]

  tags = {
    Environment = "test"
  }

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "zone" {
  name         = "${var.root_domain_name}."
  private_zone = false
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.zone.id
  name    = var.www_domain_name
  type    = "A"

  alias {
    name                   = replace(aws_cloudfront_distribution.www_distribution.domain_name, "/[.]$/", "")
    zone_id                = aws_cloudfront_distribution.www_distribution.hosted_zone_id
    evaluate_target_health = true
  }

  depends_on = [aws_cloudfront_distribution.www_distribution]
}


resource "aws_route53_record" "root_domain" {
  zone_id = data.aws_route53_zone.zone.id
  name    = var.root_domain_name
  type    = "A"

  alias {
    name                   = replace(aws_cloudfront_distribution.www_distribution.domain_name, "/[.]$/", "")
    zone_id                = aws_cloudfront_distribution.www_distribution.hosted_zone_id
    evaluate_target_health = true
  }

  depends_on = [aws_cloudfront_distribution.www_distribution]
}

resource "aws_route53_record" "cert_validation" {
  name    = tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_type
  zone_id = data.aws_route53_zone.zone.id
  records = [
    tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_value]
  ttl     = 60
}

resource "aws_route53_record" "cert_validation_alt1" {
  name    = tolist(aws_acm_certificate.certificate.domain_validation_options)[1].resource_record_name
  type    = tolist(aws_acm_certificate.certificate.domain_validation_options)[1].resource_record_type
  zone_id = data.aws_route53_zone.zone.id
  records = [
    tolist(aws_acm_certificate.certificate.domain_validation_options)[1].resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [
    aws_route53_record.cert_validation.fqdn,
    aws_route53_record.cert_validation_alt1.fqdn]
}

# Cloud Front
resource "aws_cloudfront_distribution" "www_distribution" {
  // origin is where CloudFront gets its content from.
  origin {
    domain_name = aws_s3_bucket.www.bucket_regional_domain_name
    origin_id   = aws_s3_bucket.www.bucket

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  default_root_object = var.index_file

  // All values are defaults from the AWS console.
  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    // This needs to match the `origin_id` above.
    target_origin_id       = aws_s3_bucket.www.bucket
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  // rather than the domain name CloudFront gives us.
  aliases = local.cloudfront_alias_list

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  // Here's where our certificate is loaded in!
  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.certificate.arn
    ssl_support_method  = "sni-only"
  }

   depends_on = [aws_acm_certificate.certificate, aws_acm_certificate_validation.cert]
}
