# Overview
* TODO: Separation PROD & TEST
* TODO: CI/CD 
* TODO: High-Level architecture picture
* TODO: Variables 

# Local execution guide
Testing should not be hard; therefore, you can run your scripts reproducible from your local machine. Just a few prerequisites are required:
* Docker - will be used to execute all tools like `terraform`
* AWS CLI - will be used to call the `AWS API`

### Execution of Terraform:
* Initialize Terraform: `docker run -i -t -e AWS_PROFILE=yourprofile -v $PWD:/data -v ~/.aws:/root/.aws --workdir=/data hashicorp/terraform:light  init`

* Exectute the Terraform scripts
  * **Production Environment**:
    * Select Workspace: `docker run -i -t -e AWS_PROFILE=yourprofile -v $PWD:/data -v ~/.aws:/root/.aws --workdir=/data hashicorp/terraform:light workspace select production`
    * Apply: `docker run -i -t -e AWS_PROFILE=yourprofile -v $PWD:/data -v ~/.aws:/root/.aws --workdir=/data hashicorp/terraform:light apply -var-file=environment_prod.tfvars`
  * **Test Environment**
    * Select Workspace: `docker run -i -t -e AWS_PROFILE=yourprofile -v $PWD:/data -v ~/.aws:/root/.aws --workdir=/data hashicorp/terraform:light workspace select default`
    * Apply: `docker run -i -t -e AWS_PROFILE=yourprofile -v $PWD:/data -v ~/.aws:/root/.aws --workdir=/data hashicorp/terraform:light apply -var-file=environment_test.tfvars`

# Environments
* TODO: Local
* TODO: Production
* TODO: Testing
* TODO: Why switching
# Manual actions:
* Terraform State: Create an S3 folder to store your `Terraform state` --> we do not want to create this automatically because we want to avoid to delete by accident after cleaning up with `terraform destroy`
* Hosted Zone: Create a hosted zone & add your authenticated DNS entries (also look at section `DNS & Certificate creation`. I skipped the automation of the `Hosted Zone` due to some additional modifications outside of the terraform script like adding a `mailing service`
# DNS & Certificate creation
TODO
# Mail Service 
TODO
# Outline Downstream Pipeline
TODO
